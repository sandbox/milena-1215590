<?php
/**
 * @file
 * Subclasses of the Updater class to update Drupal core knows how to update.
 * At this time, only modules and themes are supported.
 */

/**
 * Class for updating modules using FileTransfer classes via authorize.php.
 */
class BetterupdaterModuleUpdater extends ModuleUpdater {

  /**
   * Return the directory where a module should be installed.
   *
   * If the module is already installed, drupal_get_path() will return
   * a valid path and we should install it there (although we need to use an
   * absolute path, so we prepend DRUPAL_ROOT). If we're installing a new
   * module, we always want it to go into proper subdirectory, since that's
   * where all the documentation recommends users install their modules, and
   * there's no way that can conflict on a multi-site installation, since
   * the Update manager won't let you install a new module if it's already
   * found on your system, and if there was a copy in sites/all, we'd see it.
   * 
   */
  public function getInstallDirectory() {
    if ($relative_path = drupal_get_path('module', $this->name)) {
      $relative_path = dirname($relative_path);
    }
    else {
      $relative_path = variable_get('betterupdater_module_path', 'sites/all/modules');
    }
    return drupal_realpath($relative_path);
  }

  /**
   * After proper installation variable betterupdater_module path
   * is not used anymore, so Better Updater get rid of it. Variables
   * are loaded to be used quickly so there is no need for making the
   * array bigger.
   * 
   */
  public function postInstallTasks() {
    variable_del('betterupdater_module_path');
    system_rebuild_module_data();
    return array(
      l(t('Install another module'), 'admin/modules/install'),
      l(t('Enable newly added modules'), 'admin/modules'),
      l(t('Administration pages'), 'admin'),
    );
  }

}
