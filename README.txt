Better Updater 1.x
=========
The Better Updater module allows user to choose directory while installing
new modules through Update Manager.


Installation
============
1. Install and enable the module.

2. Better Updater module does not have it's own permissions; everyone
   who has the administer module permission can use Better Updater.

Contributors
============
- Milena (Milena Szalacka)
